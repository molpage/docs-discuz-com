# API 说明
## jsonapi.org

Discuz! Q 使用的是 [jsonapi.org](https://jsonapi.org/format/#query-parameters) 定义的格式，使用 [tobscure/json-api](https://github.com/tobyzerner/json-api-php) 包的实现。

## 路由分别对应

- 以 `psr-2` 基础命名规则外，下面为实践中相关命名规则。
- 以 `user` 为例， 路由和路由名称为小写复数按功能以.分开。
- 控制器按功能，列表为复数，其它单条担任可为单数驼峰命名。
- 序列化模型名为单数驼峰命名。
- 数据库模型以单数驼峰命名。
- 表名以复数命名。

下面为具体增删改查路由例子：

```php
$route->get('/users', 'users.index', ListUsersController::class);
$route->get('/users/{id}', 'users.resource', ResourceUserController::class);
$route->post('/users', 'users.create', CreateUserController::class);
$route->patch('/users/{id}', 'users.update', UpdateUserController::class);
$route->delete('/users/{id}', 'users.delete', DeleteUserController::class);
```

## 列表数据

继承 `Discuz\Api\Controller\AbstractListController` 需要指定 `$serializer` 要用于序列化模型的 `data` 方法，并实现返回模型集合的方法。该 `data` 方法接收 `Request` 对象和 `tobscure/json-api Document`。

```php
class ListUsersController extends AbstractListController
{
    public $serializer = UserSerializer::class;

    public function data(ServerRequestInterface $request, Document $document)
    {
        return User::all();
    }
}
```

## 单条数据

同列表数据一样继承 `Discuz\Api\Controller\AbstractResourceController` 并实现相关属性和方法，接收参数一样。

```php
class ResourceUserController extends AbstractResourceController
{
    public $serializer = UserSerializer::class;

    public function data(ServerRequestInterface $request, Document $document)
    {
        $body = $request->getQueryParams();
        $id = $body->get('id');
        
        return User::findOrFail($id);
    }
}
```

## 创建数据

继承 `Discuz\Api\Controller\AbstractCreateController`。这与单条数据相同，但响应状态代码将自动设置为 `201 Created`。您可以通过以下方式访问传入的 `JSON：API` POST传过来的数据可通过 `$request->getParsedBody()` 来取到 `json` 数组，并通过 `Laravel Arr` 类来获取数组相关信息。

```php
class CreateUserController extends AbstractCreateController
{
    public $serializer = UserSerializer::class;

    public function data(ServerRequestInterface $request, Document $document)
    {
        $attributes = Arr::get($request->getParsedBody(), 'data.attributes');
        
        return User::create([
            'name' => Arr::get($attributes, 'name')
        ]);
    }
}
```

## 更新数据

继承 `Discuz\Api\Controller\AbstractResourceController` 同单条数据一样，现相关属性和方法，接收参数一样，返回相关数据，获取同创建数据一样。

```php
class UpdateUserController extends AbstractCreateController
{
    public $serializer = UserSerializer::class;

    public function data(ServerRequestInterface $request, Document $document)
    {
        $attributes = Arr::get($request->getParsedBody(), 'data.attributes', []);

        $body = $request->getQueryParams();
        $attributes = $body->get('id');

        $user = User::findOrFail($id);
        
        $user->name = $attributes['name'];

        $user->save();

        return $user;
    }
}
```

## 删除数据

继承 `Discuz\Api\Controller\AbstractDeleteController` 实现 `delete` 方法，该方法接收 `Request`，默认返回空 `204 No Content` 响应。

```php
class DeleteUserController extends AbstractDeleteController

    public function delete(ServerRequestInterface $request)
    {
        $id = Arr::get($request->getQueryParams(), 'id');
        
        User::findOrFail($id)->delete();
    }
}
```

## 序列化模型说明

继承 `Discuz\Api\Serializer\AbstractSerializer` 实现 **getDefaultAttributes** 方法，该方法接收数据库模型，写好 **type** 属性。

```php
class UserSerializer extends AbstractSerializer
{
    protected $type = 'user';

    public function getDefaultAttributes($model)
    {
        return [
            'id' => $model->id,
            'username' => $model->username,
            'email' => $model->email
        ];
    }
}
```

## 自定义返回类型

实现接口 `Psr\Http\Server\RequestHandlerInterface`。

```php
<?php

namespace Discuz\Api\Controller;

use Psr\Http\Server\RequestHandlerInterface;

class AbstractSerializeController implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
       
        return new HtmlResponse();
    }
}
```