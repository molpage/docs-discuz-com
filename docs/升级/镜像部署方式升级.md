# 镜像部署方式升级  
## 概述

本文档将指导您如何升级使用腾讯云 CVM 与腾讯云 Lighthouse 镜像部署方式的 Discuz! Q 。

## 前提条件

已使用使用镜像部署方式成功部署 Discuz! Q。


## 操作指南
::: tip
腾讯云 CVM 与腾讯云 Lighthouse的 Discuz! Q 镜像基于宝塔面板搭建制作，您可以根据[常规部署升级]()。
:::